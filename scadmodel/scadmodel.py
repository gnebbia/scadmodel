import json
import re
import shutil
import sys
import zipfile
import configparser
import os
from securicad import enterprise
import networkx as nx


"""
TERMINOLOGY

Language Level -> Instance Level
classes        -> objects
associations   -> links
roles          -> roles
attack_steps   -> attack_steps
each attack steps has child attack steps or connections...
each of these child attack steps can be:
    -> internal (simple strings)
    -> external (contain at least one dot ".", e.g., app.sabotage)
    -> complex  (contain parenthesis "()", example().attackstep)
"""


def scad_connect(url: str,username: str,password: str,org: str,cacert: str):
    """
    Connect to a SecuriCAD Enterprise instance.

    Arguments:
    url               - the url of the SecuriCAD Enterprise instance
    username          - the username used to login to SecuriCAD Enterprise
    password          - the password used to login to SecuriCAD Enterprise
    org               - the organization field used to login to SecuriCAD Enterprise
    cacert            - the certificate to login to SecuriCAD Enterprise


    Return:
    A securicad enterprise client object that can be
    used to select projects or run scenarios/simulations
    """
    client = enterprise.client(
        base_url=url,
        username=username,
        password=password,
        organization=org,
        cacert=cacert if cacert else False
    )
    return client

def scad_connect_from_config(config_file: str):
    """
    Connect to a SecuriCAD Enterprise instance.

    Arguments:
    config_file     - the path to an ".ini" configuration file
                      An example config file may be structured
                      like this:

                      ``` 
                      [enterprise-client]
                      url = https://x.y.z.t
                      username = johndoe
                      password = examplepassword
                      org = myorganization
                      cacert = 
                      ```
    Return:
    A securicad enterprise client object that can be
    used to select projects or run scenarios/simulations
    """
    config = configparser.ConfigParser()
    config.read(config_file)
    client = enterprise.client(
        base_url=config["enterprise-client"]["url"],
        username=config["enterprise-client"]["username"],
        password=config["enterprise-client"]["password"],
        organization=config["enterprise-client"]["org"] if config["enterprise-client"]["org"] else None,
        cacert=config["enterprise-client"]["cacert"] if config["enterprise-client"]["cacert"] else False
    )
    return client


# Model Instance Related Functions
def get_objects(model) -> dict:
    """
    Get all the objects present in an instance model.

    Arguments:
    model           - a SecuriCAD Model object
                      this can be retrieved for example by:
                      model = model_info.get_model()

    Return:
    A dictionary representing the whole set of objects
    contained in the instance model.
    The keys of this dictionary represent the IDs of the objects
    while the value associated to each key is a dictionary
    containing all the properties of the corresponding node
    """
    return model.model["objects"]


def get_links(model) -> list:
    """
    Get all the links present in an instance model.

    Arguments:
    model           - a SecuriCAD Model object
                      this can be retrieved for example by:
                      model = model_info.get_model()

    Return:
    A list of dictionaries representing the whole set of connections
    between each object.
    Each element of the list (a dictionary) represents an edge connecting
    two objects in the instance model
    """
    return model.model["associations"]


def get_class(model,object_id: str) -> str:
    """
    Get the class related to a specific object

    Arguments:
    model           - a SecuriCAD Model object
                      this can be retrieved for example by:
                      model = model_info.get_model()
    object_id       - the ID of the object we want to retrieve the class

    Return:
    A string representing the class of the provided object ID 
    """
    objects = get_objects(model)
    try:
        return objects[object_id]["metaconcept"]
    except e:
        print("Error: object ID non existing in the current model")
        print(e)
   

def get_links_for_object(model,object_id: str) -> list:
    """
    Get the links associated to an object

    Arguments:
    model           - a SecuriCAD Model object
                      this can be retrieved for example by:
                      model = model_info.get_model()
    object_id       - the ID of the object we want to retrieve the class

    Return:
    A list of dictionaries representing the whole set of connections
    related to the provided object.
    Each element of the list (a dictionary) represents an edge connecting
    the provided object to another object in the instance model
    """
    assocs = get_links(model)
    assocs_for_entity = []
    for association in assocs:
        if object_id in [association["id1"],association["id2"]]:
            assocs_for_entity.append(association)
    return assocs_for_entity

def get_links_for_object_by_class(model,object_id: str,target_class: str) -> list:
    """
    Get the links associated to an object connecting it to other objects
    only of a specific target class

    Arguments:
    model           - a SecuriCAD Model object
                      this can be retrieved for example by:
                      model = model_info.get_model()
    object_id       - the ID of the object we want to retrieve the class
    target_class    - a string representing the class of the objects
                      we want to see the connections to

    Return:
    A list of dictionaries representing the whole set of connections
    related to the provided object with objects belonging to `target_class`.
    Each element of the list (a dictionary) represents an edge connecting
    the provided object to another object in the instance model belonging
    to `target_class`.
    """
    assocs = get_links(model)
    assocs_for_entity = []
    for association in assocs:
        if object_id in association["id1"]:
            other_object_class = get_class(association["id2"])
            if other_object_class == target_class:
                assocs_for_entity.append(association)
        elif object_id in association["id2"]:
            other_object_class = get_class(association["id1"])
            if other_object_class == target_class:
                assocs_for_entity.append(association)

    return assocs_for_entity

def get_out_links_for_object(model,object_id: str) -> list:
    """
    Get the outward links associated to an object
    NOTE: Although these functions have been implemented,
    the directionality of the links should not be taken into account

    Arguments:
    model           - a SecuriCAD Model object
                      this can be retrieved for example by:
                      model = model_info.get_model()
    object_id       - the ID of the object we want to retrieve the class

    Return:
    A list of dictionaries representing the whole set of connections
    related to the provided object pointing outward (exiting from the node).
    Each element of the list (a dictionary) represents an edge connecting
    the provided object to another object in the instance model
    """
    assocs = get_links(model)
    assocs_for_entity = []
    for association in assocs:
        if object_id == association["id1"]:
            assocs_for_entity.append(association)
    return assocs_for_entity

def get_in_links_for_object(model,object_id: str) -> list:
    """
    Get the inward links associated to an object
    NOTE: Although these functions have been implemented,
    the directionality of the links should not be taken into account

    Arguments:
    model           - a SecuriCAD Model object
                      this can be retrieved for example by:
                      model = model_info.get_model()
    object_id       - the ID of the object we want to retrieve the class

    Return:
    A list of dictionaries representing the whole set of connections
    related to the provided object pointing inward (entering from the node).
    Each element of the list (a dictionary) represents an edge connecting
    the provided object to another object in the instance model
    """
    assocs = get_links(model)
    assocs_for_entity = []
    for association in assocs:
        if object_id == association["id2"]:
            assocs_for_entity.append(association)
    return assocs_for_entity


# Language Related Functions
def read_language_spec(file_spec: str) -> dict:
    """
    Read a MAL language specification file

    Arguments:
    file_spec       - a language specification file that can be
                      provided by malc (https://github.com/mal-lang/malc)

    Return:
    A dictionary representing the whole grammar of the MAL language
    """
    with open(file_spec, 'r') as spec:
        data = spec.read()
    return json.loads(data)


def get_attacks_for_class(lang_spec: dict, object_class: str) -> dict:
    """
    Get all Attack Steps for a specific Class

    Arguments:
    lang_spec       - a dictionary representing the whole grammar of the
                      a MAL language as provided by `read_language_spec(...)`
    object_class    - a string representing the class for which we want to list
                      the possible attack steps

    Return:
    A dictionary representing the set of possible attacks for the specified
    class. Each key in the dictionary is an attack name and is associated
    to a dictionary containing other characteristics of the attack such as
    type of attack, TTC distribution, child attack steps and other information
    """
    attacks = {}

    def _get_attacks(object_class):
        nonlocal attacks
        for asset in lang_spec["assets"]:
            if asset["name"] == object_class:
                attack_steps = asset["attackSteps"]
                for atk in attack_steps:
                    if atk["type"] != "defense":
                        if not atk["name"] in attacks.keys():
                            attacks[atk["name"]] = atk
                if asset["superAsset"]:
                    _get_attacks(asset["superAsset"])

    _get_attacks(object_class)
    return attacks



def get_defenses_for_class(lang_spec: dict,object_class: str) -> dict:
    """
    Get all Defenses available for a specific Class

    Arguments:
    lang_spec       - a dictionary representing the whole grammar of the
                      a MAL language as provided by `read_language_spec(...)`
    object_class    - a string representing the class for which we want to list
                      the possible defenses 

    Return:
    A dictionary representing the set of possible defenses for the specified
    class. Each key in the dictionary is a defense name and is associated
    to a dictionary containing other characteristics of the defense
    """
    defenses = {}

    def _get_defenses(object_class):
        nonlocal defenses
        for asset in lang_spec["assets"]:
            if asset["name"] == object_class:
                attack_steps = asset["attackSteps"]
                for df in attack_steps:
                    if df["type"] == "defense":
                        if not df["name"] in defenses.keys():
                            defenses[df["name"]] = df
                if asset["superAsset"]:
                    _get_defenses(asset["superAsset"])

    _get_defenses(object_class)
    return defenses


def get_super_classes(lang_spec: dict, object_class: str) -> list:
    """
    Get all the parent classes related to a class

    Arguments:
    lang_spec       - a dictionary representing the whole grammar of the
                      a MAL language as provided by `read_language_spec(...)`
    object_class    - a string representing the class for which we want to
                      retrieve the parent classes

    Return:
    A list representing the parent classes of the provided class.
    The list is ordered from left to right, e.g., 
    ['MotherClass', 'GrandMotherClass', 'GrandGrandMotherClass']
    """
    super_classes = []

    def _get_super_classes(object_class):
        nonlocal super_classes
        for asset in lang_spec["assets"]:
            if asset["name"] == object_class and asset["superAsset"]:
                super_classes.append(asset["superAsset"])
                _get_super_classes(asset["superAsset"])

    _get_super_classes(object_class)
    return super_classes


def get_associations_for_class(lang_spec: dict, object_class: str) -> list:
    """
    Get all the possible associations related to a class

    Arguments:
    lang_spec       - a dictionary representing the whole grammar of the
                      a MAL language as provided by `read_language_spec(...)`
    object_class    - a string representing the class for which we want to
                      retrieve the possible associations

    Return:
    A list of dictionaries representing the possible associations that are
    available for a specific class
    """
    assocs = []

    def _get_assocs(object_class):
        nonlocal assocs
        for asset in lang_spec["associations"]:
            if object_class in [asset["leftAsset"],asset["rightAsset"]]:
                assocs.append({
                    "name": asset["name"],
                    "leftClass": asset["leftAsset"],
                    "leftField": asset["leftField"],
                    "rightClass": asset["rightAsset"],
                    "rightField": asset["rightField"]})
    
    classes = get_super_classes(lang_spec,object_class)
    classes.insert(0,object_class)
    
    for objclass in classes:
        _get_assocs(objclass)

    # Remove Duplicate Associations
    assocs = [dict(t) for t in {tuple(d.items()) for d in assocs}]
    return assocs

# TODO: SHOULD BE OK BUT BETTER RE-CHECK/TEST FOR EDGE CASES (TMP), do we need this?
def get_associations_by_field(lang_spec, object_class, field):
    assocs = get_associations_for_class(lang_spec, object_class)

    assocs_by_field = []
    for assoc in assocs:
        if field == assoc["leftField"]: 
            assocs_by_field.append(assoc["leftClass"])
        elif field == assoc["rightField"]:
            assocs_by_field.append(assoc["rightClass"])
    return assocs_by_field


file_spec = "scadmodel/corelang_v0.3.0.json"
file_spec = "corelang_v0.3.0.json"
lang = read_language_spec(file_spec)
super_classes = get_super_classes(lang, "UnknownSoftwareVulnerability")



client = scad_connect_from_config("/home/gnc/coa_gn.ini")
project = client.projects.get_project_by_name("test")
models = enterprise.models.Models(client)
# model_info = models.get_model_by_name(project, "easy2")
model_info = models.get_model_by_name(project, "PE test4")
# TODO get the model from simulation id
print("Project Name: ", project.name)
print("Model Name:   ", model_info.name)
model = model_info.get_model()
print("MODEL")
print(json.dumps(model.model,indent=4))

with open('petest4.json', 'w') as f:
    json.dump(model.model, f, indent=4)

print("END MODEL")

objects = get_objects(model)
print(objects)

links = get_links(model)
print(links)

object_id = "6417249322885591"
links_for_x = get_links_for_object(model,object_id)
links_for_x
in_links_for_x = get_in_links_for_object(model,object_id)
out_links_for_x = get_out_links_for_object(model,object_id)

object_class = get_class(model,object_id)

file_spec = "scadmodel/corelang_v0.3.0.json"
file_spec = "corelang_v0.3.0.json"
lang = read_language_spec(file_spec)


object_class = "SoftwareVulnerability"
sw_vuln_atks = get_attacks_for_class(lang,object_class)
print(sw_vuln_atks.keys())


object_class = "System"
sys_atks = get_attacks_for_class(lang,object_class)
print(sys_atks.keys())

object_class = "Vulnerability"
vuln_atks = get_attacks_for_class(lang,object_class)
print(vuln_atks.keys())

object_class = "Application"
app_atks = get_attacks_for_class(lang,object_class)
print(app_atks.keys())



def build_attack_step_name(attack_step: dict) -> str:
    """
    Gets the MAL (malc) language specification of an attack step
    and returns its simple string representation.
    Example:

    Input
        {'type': 'collect', 
            'lhs': {'type': 'variable', 
                    'name': 'allVulnerabilities'}, 
            'rhs': {'type': 'attackStep', 
                    'name': 'localAccessAchieved'}
        }

    Output:
        "allVulnerabilities.localAccessAchieved"

        

    Arguments:
    attack_step     - the MAL (malc) language specification of an attack step.
                      The list containing these definitions can be provided by:
                        all_atks = get_attacks_for_class(lang,"Application")
                        atks["localConnect"]["reaches"]["stepExpressions"]

    Return:
    A string representing the simple form of the attack step name similarly 
    to what can be found in MAL syntax files (i.e., ".mal" files).
    """
    atkstep_name = ""
    def _buildatk(rlhs):
        if rlhs["type"] == "attackStep":
            return atkstep_name + rlhs["name"]
        if rlhs["type"] == "collect" and "lhs" in rlhs["lhs"]:
            return atkstep_name  + _buildatk(rlhs["lhs"]) + _buildatk(rlhs["rhs"])
        if rlhs["type"] == "collect":
            return atkstep_name + _buildatk(rlhs["lhs"]) + _buildatk(rlhs["rhs"])
        elif rlhs["type"] == "field":
            return atkstep_name + rlhs["name"] + "."
        elif rlhs["type"] == "variable":
            return atkstep_name + rlhs["name"] + "()" + "."

    return _buildatk(attack_step)


def get_attack_steps_for_attack(lang: dict, object_class: str, atk_name: str) -> list:
    """
    Provides the list of child attack steps belonging to an attack.

    Arguments:
    lang            - the MAL (malc) language specification of an attack step.
    object_class    - a string representing the class for which we want to
                      retrieve the possible attack steps
    atk_name        - a string representing the attack for which we want to
                      retrieve the child attack steps (e.g., "localConnect")

    Return:
    A list of strings, each representing a child attack steps of the provided
    attack name
    """
    atksteps = []
    class_atks = get_attacks_for_class(lang,object_class)
    try:
        for atkstep in class_atks[atk_name]["reaches"]["stepExpressions"]:
            atksteps.append(build_attack_step_name(atkstep))
    except TypeError as e:
        print("exc ATTACK: " + atk_name)
        print("exc OBJ_CLASS: " + object_class)
        print(e)

    return atksteps



def is_internal_attack_step(attack_step: str) -> bool:
    """
    Returns True if the attack step is internal (or simple).
    Examples:
        "localConnect" returns True
        "app.localConnect" returns False
        "app.fw.localConnect" returns False
        "routerFirewall().localConnect" returns False

    Arguments:
    attack_step    - a string representing an attack step
                    

    Return:
    A boolean that is True if the provided attack is internal (or simple)
    """
    return not "." in attack_step

# Get all attack steps for Asset = System; atkstep = fullAccess (no variables)
object_class = "System"
sys_atks = get_attacks_for_class(lang,object_class)
print(sys_atks.keys())

atksteps = []
atk_name = "fullAccess"
for atkstep in sys_atks[atk_name]["reaches"]["stepExpressions"]:
    atksteps.append(build_attack_step_name(atkstep))

# Get all attack steps for Asset = Application; atkstep = fullAccess (variables)
object_class = "Application"
app_defs = get_defenses_for_class(lang,object_class)
print(app_defs.keys())

atksteps = []
atk_name = "fullAccess"
for atkstep in app_atks[atk_name]["reaches"]["stepExpressions"]:
    print(build_attack_step_name(atkstep))




file_spec = "corelang_v0.3.0.json"
lang = read_language_spec(file_spec)
object_class = "Network"
network_assocs = get_associations_for_class(lang,object_class)
# len(network_assocs) == 7 ? right!

# A routing Firewall is an Application
# We should get all the assocs also available for applications here
file_spec = "corelang_v0.3.0.json"
lang = read_language_spec(file_spec)
object_class = "RoutingFirewall"
rfw_assocs = get_associations_for_class(lang,object_class)
# The whole set of associations for RoutingFirewall should be 22 elements
# len(rfw_assocs) == 22 ? right!


file_spec = "corelang_v0.3.0.json"
lang = read_language_spec(file_spec)
object_class = "Application"
app_assocs = get_associations_for_class(lang,object_class)
# The whole set of associations for Application should be 21 elements
# len(app_assocs) == 21 ? right!



def insert_node(nodes, node_id, node_type, node_ttc, node_links):
    nodes.append({"id":    node_id,
                  "type":  node_type,
                  "ttc":   node_ttc,
                  "links": node_links})
    return nodes

# WRONG TMP FUNCTION IN DEV
def is_node_existing(nodes, node_id):
    for node in nodes:
        if node["id"] == node_id:
            return True


def build_link_from_internal_attack_step(obj_id: str, attack_name: str) -> str:
    """
    Returns a string representing a node ID in an attack graph.
    The format is: `assetId:atkname`

    Arguments:
    object_id       - the ID of the asset object
    attack_name     - a string representing an attack_name
                    

    Return:
    A string in the format ObjectID:AttackName
    """
    return obj_id + ":" + attack_name


def build_links_from_external_attack_step(model, lang: dict, 
        obj_id: str, 
        attack_name: str) -> list:
    """
    Returns a list of strings representing node IDs in an attack graph.
    The format is: `assetId:atkname`

    Arguments:
    model           - a SecuriCAD Model object
                      this can be retrieved for example by:
                      model = model_info.get_model()
    lang_spec       - a dictionary representing the whole grammar of the
                      a MAL language as provided by `read_language_spec(...)`
    obj_id          - the ID of the asset object
    attack_name     - a string representing an external attack name
                    

    Return:
    A list of strings in the format ObjectID:AttackName
    """
    obj_class = get_class(model,obj_id)
    complex_atk = attack_name.split(".") 
    connections = []

    def _resolve(complex_atk, obj_id):
        obj_links = get_links_for_object(model,obj_id)
        if len(complex_atk) == 2:
            assoc = complex_atk.pop(0)
            for l in obj_links:
                if obj_id == l["id1"] and l["type2"] == assoc:
                    connections.append(l["id2"] + ":" + complex_atk[0])
                elif obj_id == l["id2"] and l["type1"] == assoc:
                    connections.append(l["id1"] + ":" + complex_atk[0])
        elif len(complex_atk) > 2:
            assoc = complex_atk.pop(0)
            for l in obj_links:
                # here we could also use out-links
                if obj_id == l["id1"] and l["type2"] == assoc:
                    next_obj_id = l["id2"]
                    _resolve(complex_atk, next_obj_id)
                elif obj_id == l["id2"] and l["type1"] == assoc:
                    next_obj_id = l["id1"]
                    _resolve(complex_atk, next_obj_id)
                
    _resolve(complex_atk,obj_id)
    return connections


cplx_atk4 = "appSoftProduct.softProductVulnerabilities.localAccessAchieved"
object_id = "7219598629313512" # Application which is then connected to 2 vulns
build_links_from_external_attack_step(model,lang, obj_id, cplx_atk4)

# These can be used for other tests
cplx_atk1 = "applications.networkConnect"
cplx_atk2 = "applications.networkRequestConnect"
cplx_atk3 = "clientApplications.networkRespondConnect"



# TODO FUNCTION
# def get_attacks_for_object(model,lang,object_id):
object_id = "8176711980537409" # Network
object_id = "7219598629313512" # Application which is then connected to 2 vulns


### BEGIN MAIN 

nodes = []

for obj_id in objects:
    print("Processing Object ID: " + obj_id)
    obj_class = get_class(model, obj_id)
    print("Class: " + obj_class)
    atks = get_attacks_for_class(lang,obj_class)
    print("Attacks:")
    print(atks.keys())
    for atk_name in atks:
        print("This attack is: " + atk_name)
        print("Belonging to: " + obj_class)
        atksteps = get_attack_steps_for_attack(lang,obj_class,atk_name)
        print("Attack Steps for attack: " + atk_name + " are:")
        print(atksteps)
        links = []
        for atkstep in atksteps:
            if "()" in atkstep:
                print("Skipped Attack Step: " + atkstep)
            elif is_internal_attack_step(atkstep):
                # connected_atksteps.append(obj_id + ":" + atkstep)
                print("Internal Attack Step: " + atkstep)
                links.append(build_link_from_internal_attack_step(obj_id, atkstep))
            else:
                print("External Attack Step: " + atkstep)
                links += build_links_from_external_attack_step(model, lang, obj_id, atkstep)
        print(links)
        insert_node(nodes, obj_id + ":" + atk_name, atks[atk_name]["type"],atks[atk_name]["ttc"], links)


with open('output.json', 'w') as f:
    json.dump(nodes, f, indent=4)



nodes
g = nx.DiGraph()
for node in nodes:
    g.add_node(node["id"], type=node["type"],ttc=node["ttc"])
    for link in node['links']:
        print("Connecting " + node["id"] + " to " + link)
        g.add_edge(node["id"],link)


# Access node properties
g.nodes['929864580059290:exploitWithEffort']

g.edges()
import matplotlib.pyplot as plt
from pyvis.network import Network

net = Network(notebook=True)

net.from_nx(g)
net.show("example.html")



nx.draw(g)
plt.show()
plt.savefig("path.png")
### END MAIN 






for l in links:
    if l["type2"] == complex_atk[0]:
        linkz.append(l["id2"] + ":" + complex_atk[1])
        

assocs = get_associations_by_field(lang,obj_class,complex_atk[0])
print(assocs)
cassocs = get_associations_for_class(lang,obj_class)

links = get_links_for_object(model,obj_id)
links = get_links_for_object_by_class

assocs2 = get_associations_for_object(model,obj_id)

for assoc in assocs2:
    if complex_atk[0] == assoc["type"] and assoc["id"]


def get_associations_for_object(model,obj_id):
    links = get_links_for_object(model,obj_id)
    assocs = []
    for link in links:
        if obj_id == link["id1"]:
            assocs.append({"id": link["id2"],
                           "type": link["type2"]})
        elif obj_id == link["id2"]:
            assocs.append({"id": link["id1"],
                           "type": link["type1"]})
    return assocs


nodes = []
# Loop for all the attacks in the category:
# create a node for each attack
for atk in net_atks:
    print(net_atks[atk]["name"])
    atk_name = net_atks[atk]["name"]

    connected_atksteps = []
    atksteps = get_attack_steps_for_attack(lang,object_class,atk_name)
    for atkstep in atksteps:
        if is_internal_attack_step(atkstep):
            connected_atksteps.append(object_id + ":" + atkstep)
        else:
            resolve_atkstep(object_id,atkstep)


insert_node(nodes,
        object_id + ":" + atk_name,
        net_atks[atk]["type"],
        net_atks[atk]["ttc"],
        connected_atksteps)





if __name__ == "__main__":
    # Create an authenticated enterprise client
    client = scad_connect_from_config(sys.argv[1])
    project = client.projects.get_project_by_name("test")
    models = enterprise.models.Models(client)

    model_info = models.get_model_by_name(project, "easy2")
    # TODO get the model from simulation id
    print("Project Name: ", project)
    print("Model Name:   ", model_info.name)
    model = model_info.get_model()
    print("MODEL")
    print(json.dumps(model.model,indent=4))
    print("END MODEL")

    entities = get_entities(model)
    print(entities)


