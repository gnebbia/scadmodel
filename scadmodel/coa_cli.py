from securicad import enterprise
import json
import re
import shutil
import sys
import zipfile
import configparser
import os


def scad_connect(url,username,password,org,cacert):
    client = enterprise.client(
        base_url=url,
        username=username,
        password=password,
        organization=org,
        cacert=cacert if cacert else False
    )
    return client

def scad_connect_from_config(config_file):
    config = configparser.ConfigParser()
    config.read(config_file)
    client = enterprise.client(
        base_url=config["enterprise-client"]["url"],
        username=config["enterprise-client"]["username"],
        password=config["enterprise-client"]["password"],
        organization=config["enterprise-client"]["org"] if config["enterprise-client"]["org"] else None,
        cacert=config["enterprise-client"]["cacert"] if config["enterprise-client"]["cacert"] else False
    )
    return client


def get_nodes(model):
    return model.model["objects"]


def get_rels(model):
    return model.model["associations"]


def get_node_type(model,node_id):
    nodes = get_nodes(model)
    try:
        return nodes[node_id]["metaconcept"]
    except e:
        print("Error: node ID non existing in the current model")
        print(e)
   


def get_rels_for_node(model,node_id):
    assocs = get_rels(model)
    assocs_for_entity = []
    for association in assocs:
        if node_id in [association["id1"],association["id2"]]:
            assocs_for_entity.append(association)
    return assocs_for_entity

def get_out_rels_for_node(model,node_id):
    assocs = get_rels(model)
    assocs_for_entity = []
    for association in assocs:
        if node_id == association["id1"]:
            assocs_for_entity.append(association)
    return assocs_for_entity

def get_in_rels_for_node(model,node_id):
    assocs = get_rels(model)
    assocs_for_entity = []
    for association in assocs:
        if node_id == association["id2"]:
            assocs_for_entity.append(association)
    return assocs_for_entity


def get_lang_spec(file_spec):
    with open(file_spec, 'r') as spec:
        data = spec.read()
    return json.loads(data)


def get_attacks_for_node_type(lang_spec,node_type):
    attacks = {}

    def _get_attacks(node_type):
        nonlocal attacks
        for asset in lang_spec["assets"]:
            if asset["name"] == node_type:
                attack_steps = asset["attackSteps"]
                for atk in attack_steps:
                    if atk["type"] != "defense":
                        if not atk["name"] in attacks.keys():
                            attacks[atk["name"]] = atk
                if asset["superAsset"]:
                    _get_attacks(asset["superAsset"])

    _get_attacks(node_type)
    return attacks

def get_defenses_for_node_type(lang_spec,node_type):
    defenses = {}

    def _get_defenses(node_type):
        nonlocal defenses
        for asset in lang_spec["assets"]:
            if asset["name"] == node_type:
                attack_steps = asset["attackSteps"]
                for df in attack_steps:
                    if df["type"] == "defense":
                        if not df["name"] in defenses.keys():
                            defenses[df["name"]] = df
                if asset["superAsset"]:
                    _get_defenses(asset["superAsset"])

    _get_defenses(node_type)
    return defenses


client = scad_connect("https://34.91.238.96/","Giuseppe","Nebbione12345","coa",None)
# client = scad_connect("url","Giuseppe","","c",None)
project = client.projects.get_project_by_name("test")
models = enterprise.models.Models(client)
model_info = models.get_model_by_name(project, "easy2")
# TODO get the model from simulation id
print("Project Name: ", project)
print("Model Name:   ", model_info.name)
model = model_info.get_model()
print("MODEL")
print(json.dumps(model.model,indent=4))
print("END MODEL")

nodes = get_nodes(model)
print(nodes)

rels = get_rels(model)
print(rels)

node_id = "1413488895170795"
rels_for_x = get_rels_for_node(model,node_id)
rels_for_x
in_rels_for_x = get_in_rels_for_node(model,node_id)
out_rels_for_x = get_out_rels_for_node(model,node_id)

node_type = get_node_type(model,node_id)

file_spec = "corelang_v0.3.0.json"
lang = get_lang_spec(file_spec)

node_type = "Vulnerability"
vuln_atks = get_attacks_for_node_type(lang,node_type)
print(vuln_atks.keys())

node_type = "SoftwareVulnerability"
sw_vuln_atks = get_attacks_for_node_type(lang,node_type)
print(sw_vuln_atks.keys())

node_type = "Application"
app_atks = get_attacks_for_node_type(lang,node_type)
print(app_atks.keys())

node_type = "Application"
app_defs = get_defenses_for_node_type(lang,node_type)
print(app_atks.keys())


# get_attacks_for_node(lang,node_id)
node_type = get_node_type(model,node_id)
out_rels = get_out_rels_for_node(model,node_id)
atks = get_attacks_for_node_type(lang,node_type)
    


lang['associations']



if __name__ == "__main__":
    # Create an authenticated enterprise client
    client = scad_connect_from_config(sys.argv[1])
    project = client.projects.get_project_by_name("test")
    models = enterprise.models.Models(client)

    model_info = models.get_model_by_name(project, "easy2")
    # TODO get the model from simulation id
    print("Project Name: ", project)
    print("Model Name:   ", model_info.name)
    model = model_info.get_model()
    print("MODEL")
    print(json.dumps(model.model,indent=4))
    print("END MODEL")

    entities = get_entities(model)
    print(entities)


