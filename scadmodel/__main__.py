# -*- encoding: utf-8 -*-
# scadmodel v0.1.0
# Convert a SecuriCAD instance project to a reusable model
# Copyright © 2021, Giuseppe Nebbione.
# See /LICENSE for licensing information.

"""
Main routine of scadmodel.

:Copyright: © 2021, Giuseppe Nebbione.
:License: GPLv3 (see /LICENSE).
"""
from scadmodel.main import main


__all__ = ('main',)



if __name__ == '__main__':
    main()
