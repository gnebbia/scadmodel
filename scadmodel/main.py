# -*- encoding: utf-8 -*-
# scadmodel v0.1.0
# Convert a SecuriCAD instance project to a reusable model
# Copyright 2021, Giuseppe Nebbione.
# See /LICENSE for licensing information.

"""
INSERT MODULE DESCRIPTION HERE.

:Copyright: 2021, Giuseppe Nebbione.
:License: GPLv3 (see /LICENSE).
"""

__all__ = ()

import sys
from scadmodel.cl_parser import parse_args



def main():
    """Main routine of scadmodel."""

    print("Hello World")
    args = parse_args(sys.argv[1:])
    cmd_params = vars(args)
    print(cmd_params)
