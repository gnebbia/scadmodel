def get_model_from_zip():
    # download the model
    temp = True
    if not temp:
        pass
    else:
        datapath = 'data-models'
        if not os.path.exists(datapath):
            os.makedirs(datapath)
        model_path = "data-models/temp.sCAD"
        scad_dump = modelinfo.get_scad()
        print("model downloaded")
        f1 = open(model_path, "wb")
        f1.write(scad_dump)
        f1.close()

        # unzip the model
        model_dir_path = model_path[:model_path.rindex('/')]
        model_file_name = model_path[model_path.rindex('/') + 1:model_path.rindex('.')]
        unzip_dir = "scad_dir"
        unzip_dir_path = "{}/{}".format(model_dir_path, unzip_dir)
        with zipfile.ZipFile(model_path, 'r') as zip_ref:
            zip_ref.extractall(unzip_dir_path)
        eom_path = "{}/{}.eom".format(unzip_dir_path, modelinfo.name)
        print("model unzipped in  -- ", unzip_dir_path)

        # xml parsing
        with open(eom_path, 'rt') as f:
            tree = ET.parse(f)
            root = tree.getroot()


        model_dict_list = []

        for entity in root.iter("objects"):
            model_dict = {}
            model_dict["name"] = entity.attrib['name']
            model_dict["metaConcept"] = entity.attrib['metaConcept']
            model_dict["exportedId"] = entity.attrib['exportedId']
            model_dict["attributesJsonString"] = json.loads(entity.attrib['attributesJsonString'])
            model_dict_list.append(model_dict)
