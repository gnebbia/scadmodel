.PHONY: default, lint

default:
	python -m scadmodel 
spell:
	codespell . --ignore-words-list=hist --skip=./.* --quiet-level=2 || true
lint:
	pylint scadmodel 
pep8:
	autopep8 scadmodel  --in-place --recursive --aggressive --aggressive
clean:
	rm -rf build/ dist/ scadmodel_manager.egg-info/
test:
	codespell . --ignore-words-list=hist --quiet-level=2
	flake8 . --count --select=E9,F63,F7,F82 --show-source --statistics
	pytest
reinstall:
	pip uninstall scadmodel 
	pyenv rehash
	pip install .
	pyenv rehash
