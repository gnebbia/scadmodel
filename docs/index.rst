===============================
scadmodel
===============================

.. toctree::
   :maxdepth: 2

   README for scadmodel <README>
   CONTRIBUTING
   LICENSE
   CHANGELOG

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
